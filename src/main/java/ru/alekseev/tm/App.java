package ru.alekseev.tm;

import ru.alekseev.tm.constant.CommandConst;
import ru.alekseev.tm.manager.ProjectManager;
import ru.alekseev.tm.manager.TaskManager;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class App {

    static ProjectManager projectManager = new ProjectManager();
    static TaskManager taskManager = new TaskManager();
    static BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    public static void main(String[] args) throws Exception {
        System.out.println("*** WELCOME TO TASK MANAGER ***");

        while (true) {
            String input = reader.readLine();
            switch (input) {
                case CommandConst.HELP:
                    showCommands();
                    break;
                case CommandConst.SHOW_PROJECTS:
                    showProjects();
                    break;
                case CommandConst.ADD_PROJECT:
                    addProject();
                    break;
                case CommandConst.UPDATE_PROJECT:
                    updateProject();
                    break;
                case CommandConst.DELETE_PROJECT:
                    deleteProject();
                    break;
                case CommandConst.CLEAR_PROJECTS:
                    clearProjects();
                    break;
                case CommandConst.SHOW_TASKS:
                    showTasks();
                    break;
                case CommandConst.ADD_TASK:
                    addTask();
                    break;
                case CommandConst.CLEAR_TASKS:
                    clearTasks();
                    break;
                case CommandConst.EXIT:
                    System.exit(0);
                    break;
                default:
                    showCommands();
                    break;
            }
            System.out.println();
        }
    }

    static void showCommands() {
        System.out.println(CommandConst.HELP + " - Show all commands");

        System.out.println(CommandConst.SHOW_PROJECTS + " - Show all projects");
        System.out.println(CommandConst.ADD_PROJECT + " - Add new project");
        System.out.println(CommandConst.UPDATE_PROJECT + " - Update project");
        System.out.println(CommandConst.DELETE_PROJECT + " - Delete project by number");
        System.out.println(CommandConst.CLEAR_PROJECTS + " - Delete all projects");

        System.out.println(CommandConst.SHOW_TASKS + " - Show all tasks");
        System.out.println(CommandConst.ADD_TASK + " - Add new task");
        System.out.println(CommandConst.CLEAR_TASKS + " - Delete all tasks");

        System.out.println(CommandConst.EXIT + " - Stop the Project Manager App");
    }

    private static void showProjects() {
        System.out.println("[LIST OF ALL PROJECTS]");
        projectManager.showProjects();
    }

    private static void addProject() throws IOException {
        System.out.println("[ADDING OF NEW PROJECT]");
        System.out.println("ENTER NAME");
        String ss = reader.readLine();
        projectManager.addProject(ss);
        System.out.println("[OK]");
    }

    private static void updateProject() throws IOException {
        System.out.println("[UPDATING OF PROJECT]");
        System.out.println("ENTER NUMBER OF PROJECT");
        int projectNumber = Integer.parseInt(reader.readLine());
        System.out.println("ENTER NEW NAME");
        String newName = reader.readLine();
        projectManager.changeProject(projectNumber, newName);
        System.out.println("[OK]");
    }

    private static void deleteProject() throws IOException {
        System.out.println("[DELETING OF PROJECT]");
        System.out.println("ENTER NUMBER OF PROJECT");
        int projectNumber = Integer.parseInt(reader.readLine());
        String projectID = projectManager.getProjectID(projectNumber);
        taskManager.clearTasks(projectID);
        projectManager.deleteProject(projectNumber);
        System.out.println("[OK]");
    }

    private static void clearProjects() {
        projectManager.clearProjects();
        System.out.println("[ALL PROJECTS DELETED]");
    }

    private static void showTasks() throws Exception {
        System.out.println("[LIST OF ALL TASKS IN PROJECT]");
        System.out.println("ENTER PROJECT NUMBER");
        int number = Integer.parseInt(reader.readLine());
        String projectID = projectManager.getProjectID(number - 1);
        taskManager.showTasks(projectID);
    }

    private static void addTask() throws IOException {
        System.out.println("[ADDING OF NEW TASK]");
        System.out.println("ENTER PROJECT NUMBER");
        int number = Integer.parseInt(reader.readLine());
        String projectID = projectManager.getProjectID(number - 1);
        System.out.println("[OK]");
        System.out.println("ENTER TASK NAME");
        String name = reader.readLine();
        taskManager.addTask(projectID, name);
        System.out.println("[OK]");
    }

    private static void clearTasks() throws IOException {
        System.out.println("[DELETING OF TASKS BY PROJECT NUMBER]");
        System.out.println("ENTER PROJECT NUMBER");
        int number = Integer.parseInt(reader.readLine());
        String projectID = projectManager.getProjectID(number - 1);
        taskManager.clearTasks(projectID);
        System.out.println("[ALL TASKS DELETED]");
    }
}
