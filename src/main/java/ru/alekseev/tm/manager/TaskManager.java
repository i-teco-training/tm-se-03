package ru.alekseev.tm.manager;

import ru.alekseev.tm.entity.Task;

import java.util.ArrayList;
import java.util.List;

public class TaskManager {
    private List<Task> tasks = new ArrayList<>();

    public void showTasks(String projectNumber) {
        int count = 0;

        for (int i = 0; i < tasks.size(); i++) {
            if (projectNumber != null && tasks.get(i).getProjectID() != null && tasks.get(i).getProjectID().equals(projectNumber)) {
                System.out.println(tasks.get(i).getName());
                count++;
            }
        }

        if (count == 0)
            System.out.println("LIST OF TASKS IS EMPTY");
    }

    public void addTask(String projectID, String name) {
        Task task = new Task();
        task.setProjectID(projectID);
        task.setName(name);
        tasks.add(task);
    }

    public void clearTasks(String projectID) {
        for (Task task : tasks) {
            if (projectID != null && task != null && task.getProjectID().equals(projectID)) {
                tasks.remove(task);
            }
        }
    }
}
