package ru.alekseev.tm.manager;

import ru.alekseev.tm.entity.Project;

import java.util.ArrayList;
import java.util.List;

public class ProjectManager {

    private List<Project> projects = new ArrayList<>();

    public void showProjects() {
        if (projects.size() == 0)
            System.out.println("LIST OF PROJECTS IS EMPTY");

        for (int i = 0; i < projects.size(); i++) {
            System.out.print(i + 1);
            System.out.println(") " + projects.get(i).getName());
        }
    }

    public void addProject(String name) {
        Project project = new Project();
        project.setName(name);
        projects.add(project);
    }

    public void changeProject(int projectNumber, String name) {
        Project project = new Project();
        project.setName(name);
        projects.set(projectNumber - 1, project);
    }

    public void deleteProject(int projectNumber) {
        projects.remove(projectNumber - 1);
    }

    public void clearProjects() {
        projects.clear();
    }

    public String getProjectID(int projectIndex) {
        return projects.get(projectIndex).getId();
    }
}
